# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .inventory import InventoryLine


def register():
    Pool.register(
        InventoryLine,
        module='stock_inventory_cost', type_='model')
