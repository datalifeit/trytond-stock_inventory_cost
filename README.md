datalife_stock_inventory_cost
=============================

The stock_inventory_cost module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_inventory_cost/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_inventory_cost)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
