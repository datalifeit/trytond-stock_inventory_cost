from trytond.pool import PoolMeta
from trytond.model import fields
from decimal import Decimal
from trytond.pyson import Eval


class InventoryLine(metaclass=PoolMeta):
    __name__ = 'stock.inventory.line'

    diff = fields.Function(
        fields.Float('Difference', digits=(16, Eval('unit_digits', 2)),
            depends=['unit_digits']),
        'on_change_with_diff')
    cost_amount = fields.Function(fields.Numeric(
        'Cost', digits=(16, Eval('currency_digits', 2)),
        readonly=True, depends=['currency_digits']), 'get_cost_amount')
    currency_digits = fields.Function(
        fields.Integer('Currency Digits'), 'get_currency_digits')

    @fields.depends('quantity', 'expected_quantity')
    def on_change_with_diff(self, name=None):
        if self.quantity is not None and self.expected_quantity is not None:
            return self.quantity - self.expected_quantity

    def get_cost_amount(self, name=None):
        if self.moves:
            sign = -1 if self.diff < 0 else 1
            res = sign * sum(Decimal(move.quantity) * move.cost_price
                for move in self.moves)
        elif self.diff is not None:
            res = Decimal(self.diff or '0.0') * self.product.cost_price
        else:
            return None
        return self.inventory.company.currency.round(res)

    def get_currency_digits(self, name):
        return self.inventory.company.currency.digits
